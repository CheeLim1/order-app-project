export function formatPrice(price) {
  return price.toLocaleString("en-MY", {
    style: "currency",
    currency: "MYR"
  });
}

export const foodItems = [
  {
    name: "Brown Sugar Milk Tea",
    img: "/img/brown_sugar_tea.jpg",
    section: "Tea",
    price: 10
  },
  {
    name: "Matcha Milk Tea",
    img: "/img/matcha_tea.jpg",
    section: "Tea",
    price: 12.6
  },
  {
    name: "Strawberry Bubble Tea",
    img: "/img/strawberry_bubble_tea.jpg",
    section: "Tea",
    price: 15
  },
  {
    img: "/img/moobar.png",
    name: "Bubble Milk Tea",
    section: "Tea",
    price: 6.9
  },
  {
    img: "/img/lemon_tea.jpg",
    name: "Ice Lemon Tea",
    section: "Tea",
    price: 4.5
  },
  { 
    img: "/img/gyro.jpeg", 
    name: "Beef Kebab", 
    section: "Sandwich", 
    price: 7.5 },
  {
    img: "/img/chicken-fingers.jpeg",
    name: "Pop Corn Chicken",
    section: "Sides",
    price: 6
  },
  {
    img: "/img/fries.jpeg",
    name: "Fries",
    section: "Sides",
    price: 3
  },
  {
    img: "/img/soda.jpg",
    price: 1.5,
    name: "Soda",
    section: "Drinks",
    choices: ["Coke", "Sprite", "Root Beer"]
  }
];

export const foods = foodItems.reduce((res, food) => {
  if (!res[food.section]) {
    res[food.section] = [];
  }
  res[food.section].push(food);
  return res;
}, {});
