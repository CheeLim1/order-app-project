var express = require("express");
var router = express.Router();

router.get("/orderpayment", function(req, res,next) {
    console.log("API IN -> /orderpayment");
    try {
        res.send({
            status: 200,
            message: "Order Confirmed"
        });
    } catch (error) {
        res.send({
            status:500,
            message: "Order Failed, Please try again!"
        });
        console.log(error);
  }
});

module.exports = router;